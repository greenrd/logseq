{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
module Main where

import Control.Monad.Writer.Lazy (runWriter, tell, Writer)
import Data.LogSeq.Kanban.Board (Board(..), boardCards, boardsToBulletList, parseBoards)
import Data.LogSeq.Kanban.Cards
import Data.LogSeq.Kanban.Types (Card(..))
import Data.Text (pack, unpack)
import Lens.Micro ((.~), (%~))
import Lens.Micro.GHC ()
import Lens.Micro.Internal (At(..), Index, Ixed(..), IxValue)
import Lens.Micro.Type (Lens')
import Text.Pandoc.JSON
import Text.Pretty.Simple (pPrint)
import Universum hiding (find, Lens', Traversal', (.~), (%~), view)

type instance Index Board = Text
type instance IxValue Board = Card

instance Ixed Board where

parseName :: Text -> [Inline]
parseName = intersperse Space . map Str . words

findLens :: (a -> Bool) -> [a] -> Maybe (Lens' [a] (Maybe a))
findLens _ [] = Nothing
findLens p (h:t)
    | p h       = Just $ \f (h':t') -> (\r -> maybe id (:) r t') <$> f (pure h')
    | otherwise = case findLens p t of
                    Nothing   -> Nothing
                    Just lens -> Just $ tailLens . lens
    where
        tailLens :: Lens' [a] [a]
        tailLens = \g (h':t') -> (h' :) <$> g t'

instance At Board where
    at :: Text -> Lens' Board (Maybe Card)
    at cId f b@(Board {..}) =
        case findLens ((== pure cId) . cardId) $ cardsToList _boardCards of
            Just lens -> boardCards (map listToCards . lens f . cardsToList) b
            Nothing -> 
                (\mv -> maybe id ((boardCards %~) . insert) mv b) <$> f Nothing

garbageCollect :: Board -> Board
garbageCollect = (boardCards . finishedCards) %~ (filter $ not . cardArchived)

blockFilter :: [String] -> Block -> IO Block
blockFilter args b = do
    ["mv", cId, bName] <- pure args
    boards <- either ((pPrint b >>) . fail . unpack . toText) pure $ parseBoards b
    pure . boardsToBulletList . execState (moveCardAround (pack cId) $ pack bName) $ garbageCollect <$> boards
    where
        moveCardAround :: Text -> Text -> State [Board] ()
        moveCardAround cId bName = maybe (pure ()) (insertCardInto bName cId) =<< extractFirstCard cId
        insertCardInto :: Text -> Text -> Card -> State [Board] ()
        insertCardInto bName cId card = state $ ((), ) . map insertCardIntoSpecificBoard
            where
                insertCardIntoSpecificBoard board
                    | boardName board == bNameP = (at cId .~ pure card) board
                    | otherwise                 = board
                bNameP = parseName bName
        extractFirstCard :: Text -> State [Board] (Maybe Card)
        extractFirstCard cId = state $ swap . map getFirst . runWriter . mapM extractCardFromBoard
            where
                -- also removes duplicates
                extractCardFromBoard :: (Applicative f, Monoid (f Card)) => Board -> Writer (f Card) Board
                extractCardFromBoard b = at cId (($> Nothing) . maybe (pure ()) (tell . pure)) b

pandocFilter :: [String] -> Pandoc -> IO Pandoc
pandocFilter args p@(Pandoc m (h:_)) = (\b -> Pandoc m [b]) <$> blockFilter args h

main :: IO ()
main = toJSONFilter pandocFilter