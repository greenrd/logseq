{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeSynonymInstances #-}
module Data.LogSeq.Kanban.Types where

import Control.Lens (re)
import Control.Lens.Prism (Prism', prism')
import qualified Data.Map.Strict as Map
import Data.Text (pack, stripSuffix, toLower, toTitle)
import Debug.Pretty.Simple (pTraceShowId)
import Lens.Micro (_2, (^.), (^?))
import Text.Pandoc.Definition (Block(..), Inline(..))
import Universum hiding (_2, (^.), (^?))

data Card = Card { cardDescription :: [Inline]
                 , cardProp :: [Inline]
                 , cardDuration :: [Inline]
                 , cardSubtasks :: [[Inline]]
                 , cardArchived :: Bool
                 , cardId :: Maybe Text
                 }

data Prop = Prop { propName  :: Text 
                 , propValue :: [Inline]
                 }

propToPair :: Prop -> (Text, [Inline])
propToPair (Prop {..}) = (propName, propValue)

instance Eq Prop where
  x == y = compare x y == EQ

instance Ord Prop where
  compare = flip . comparing $ subst . propName
    where
      subst "id" = "a"
      subst n    = n

propPresentation :: Prism' [Inline] Prop
propPresentation = prism' (\p -> Str (propName p <> "::") : Space : propValue p)
                          (\line -> do
                            (Str firstWord) : Space : rest <- pure line
                            pn <- "::" `stripSuffix` firstWord
                            pure $ Prop pn rest)

hardBreakUnlines :: [[Inline]] -> [Inline]
hardBreakUnlines = intercalate [LineBreak]

split :: (a -> Bool) -> [a] -> ([a], [a])
split p = map (dropWhile p) . break p

softBreakLines :: [Inline] -> [[Inline]]
softBreakLines = unfoldr $ \list ->
   split isBreak list <$ guard (not $ null list)
   --mfilter (const . not $ null list) $ pure . fmap (dropWhile (== SoftBreak)) $ break (== SoftBreak) list
   --if null list then Nothing else Just . fmap (dropWhile (== SoftBreak)) $ break (== SoftBreak) list
  where
    isBreak x = x == SoftBreak || x == LineBreak
--softBreakIso :: Iso' [Inline] [[Inline]]
--softBreakIso = iso softBreakLines softBreakUnlines

boolPresentation :: Prism' Text Bool
boolPresentation = prism' (toLower . show) $ readMaybe . toTitle

omitFalse :: Bool -> Maybe Bool
omitFalse b = b <$ guard b

cardToBullet :: Card -> NonEmpty Block
cardToBullet (Card {..}) =
  Plain (hardBreakUnlines theLines) :| subtaskBlocks
  where
    subtaskBlocks = if null cardSubtasks then [] else [BulletList $ (:[]) . Plain <$> cardSubtasks]
    theLines = cardDescription : map (^. re propPresentation) (sort props)
    props = optionalProp "archived" ((^. re boolPresentation) <$> omitFalse cardArchived) $ 
            optionalProp "id" cardId [ Prop "prop" cardProp
                                     , Prop "duration" cardDuration
                                     ]
    optionalProp name = maybe id (\v -> (Prop name [Str v] :))

data ParseError = ParseError { expected :: Text
                             , actual :: Text }

instance ToText ParseError where
  toText (ParseError {..}) = "Expected: " <> expected <> " but got: " <> actual

instance ToText Inline where
  toText (Str s) = s
  toText Space = " "

instance ToText [Inline] where
  toText = mconcat . map toText

type ParseResult a = Either ParseError a

instance MonadFail (Either ParseError) where
  fail s = Left $ ParseError { expected = pack s, actual = "???" }

parseProps :: [[Inline]] -> ParseResult (Map Text [Inline])
parseProps = map (Map.fromList . map propToPair) . mapM parseProp
  where
    parseProp :: [Inline] -> ParseResult Prop
    parseProp line = maybeToRight (ParseError { expected = "propertyName:: propertyValue", actual = toText line }) $ line ^? propPresentation

parsePropsBlock :: Block -> ParseResult ([Inline], Map Text [Inline])
parsePropsBlock block =
  _2 parseProps =<< (do
    theLines <- extractLines block
    maybeToRight (ParseError { expected = "properties", actual = "''" }) . uncons $ softBreakLines theLines)
    where
      extractLines (Plain theLines) = pure theLines
      extractLines (Para theLines) = pure theLines
      extractLines block = Left $ ParseError { expected = "properties", actual = pack $ show block }

extractStr :: MonadFail m => [Inline] -> m Text
extractStr s =
    do 
      [Str x] <- pure s
      pure x

parseBool :: [Inline] -> ParseResult Bool
parseBool i = do
  t <- extractStr i
  maybeToRight (ParseError { expected = "[true | false]", actual = t }) $ t ^? boolPresentation

parseCard :: NonEmpty Block -> ParseResult Card
parseCard (block :| t) =
  do
    (cardDesc, propMap) <- parsePropsBlock block
    let lookupProp p = maybeToRight (ParseError { expected = p <> "::", actual = pack (show propMap) }) $ Map.lookup p propMap
    prop <- lookupProp "prop"
    duration <- pTraceShowId <$> lookupProp "duration"
    archived <- maybe (pure False) parseBool $ Map.lookup "archived" propMap
    subtasks <- extractSubtasks
    pure . Card cardDesc prop duration subtasks archived $ extractStr =<< Map.lookup "id" propMap
    where
      extractSubtasks :: ParseResult [[Inline]]
      extractSubtasks
        | null t = pure []
        | otherwise = 
            do
                [BulletList subtasks] <- pure t
                mapM extractSubtask subtasks
      extractSubtask :: [Block] -> ParseResult [Inline]
      extractSubtask st =
        do
          [Plain subtask] <- pure st
          pure subtask

parseCards :: [[Block]] -> ParseResult [Card]
parseCards = mapM $ parseCard <=< (maybeToRight (ParseError { expected = "card(s)", actual = "''" } ) . nonEmpty)