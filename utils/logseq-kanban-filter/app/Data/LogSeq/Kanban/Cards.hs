{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Data.LogSeq.Kanban.Cards (Cards, cardsToList, finishedCards, insert, listToCards) where

import Lens.Micro ((%~), Lens', lens)
import Lens.Micro.TH (makeLenses)
import Data.LogSeq.Kanban.OMultiMap (OMultiMap)
import qualified Data.LogSeq.Kanban.OMultiMap as OMultiMap
import Data.LogSeq.Kanban.Types
import Text.Pandoc.JSON
import Universum hiding ((%~), Lens')

newtype Cards = Cards { _cardsMap :: OMultiMap [Inline] Card }

makeLenses ''Cards

-- Not an iso, because it does a "group by", which gathers up ungrouped cards
-- cardsList :: Iso' Cards [Card]

cardsToList :: Cards -> [Card]
cardsToList = OMultiMap.values . _cardsMap

listToCards :: [Card] -> Cards
listToCards = Cards . OMultiMap.fromList . map cardPair
    where
        cardPair :: Card -> ([Inline], Card)
        cardPair c = (cardProp c, c)

insert :: Card -> Cards -> Cards
insert card = cardsMap %~ OMultiMap.insert (cardProp card) card

finishedState :: [Inline]
finishedState = [Str "Finished"]

finishedCards :: Lens' Cards [Card]
finishedCards = cardsMap . lens (OMultiMap.lookup finishedState) (flip $ OMultiMap.replace finishedState)