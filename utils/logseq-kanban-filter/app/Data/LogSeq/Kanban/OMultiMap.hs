module Data.LogSeq.Kanban.OMultiMap (OMultiMap, fromList, insert, lookup, replace, values) where

import qualified Data.Map.Ordered.Strict as OMap
import Data.Map.Ordered.Strict ((<|), Bias(..), L, OMap)
import Universum hiding (fromList)

type OMultiMap k v = Bias L (OMap k [v])

values :: OMultiMap k v -> [v]
values = (snd =<<) . OMap.assocs . unbiased

singleton :: k -> v -> OMultiMap k v
singleton k v = Bias $ OMap.singleton (k, [v])

insert :: Ord k => k -> v -> OMultiMap k v -> OMultiMap k v
insert k v = (<> singleton k v)

replace :: Ord k => k -> [v] -> OMultiMap k v -> OMultiMap k v
replace k vs = Bias . ((k, vs) <|) . unbiased

lookup :: Ord k => k -> OMultiMap k v -> [v]
lookup k = fromMaybe [] . OMap.lookup k . unbiased

fromList :: Ord k => [(k, v)] -> OMultiMap k v
fromList = mconcat . (uncurry singleton <$>)