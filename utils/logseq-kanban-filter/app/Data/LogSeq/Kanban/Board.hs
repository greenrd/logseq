{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
module Data.LogSeq.Kanban.Board where

import Control.Lens (re)
import Control.Monad.Extra (unfoldM)
import Data.LogSeq.Kanban.Types
import Data.LogSeq.Kanban.Cards
import Lens.Micro.TH (makeLenses)
import Text.Pandoc.Definition (Block(..), Inline(..))
import Lens.Micro ((^.))
import Universum hiding (_2, (^.), (^?), view)
import qualified Data.Map.Strict as Map

data Board = Board { boardName :: [Inline]
    , boardId :: Text
    , boardConfigs :: [Inline]
    , boardCollapsed :: Bool
    , _boardCards :: Cards
    }

makeLenses ''Board

boardToBlocks :: Board -> [[Block]]
boardToBlocks (Board {..}) = 
  [ [Plain $ intersperse Space [Str "{{renderer", Str ":kboard,", Str (boardId <> ","), Str "prop}}"]]
  , [Plain $ hardBreakUnlines theLines, BulletList $ toList . cardToBullet <$> cardsToList _boardCards]
  ]
  where
    theLines = boardName : map (^. re propPresentation) [ Prop "id" [Str boardId]
                                                        , Prop "configs" boardConfigs
                                                        , Prop "collapsed" [Str $ boardCollapsed ^. re boolPresentation]
                                                        ]

boardsToBulletList :: [Board] -> Block
boardsToBulletList bs = BulletList $ boardToBlocks =<< bs

parseBoard :: [Block] -> ParseResult Board
parseBoard blocks =
  do
    [boardBit, BulletList cardsBit] <- pure blocks
    (boardName, propMap) <- parsePropsBlock boardBit
    let lookupProp p = maybeToRight (ParseError { expected = p <> "::", actual = "no such property" }) $ Map.lookup p propMap
    boardConfigs <- lookupProp "configs"
    [Str boardId] <- lookupProp "id"
    boardCollapsed <- lookupProp "collapsed"
    boardCards <- parseCards cardsBit
    collapsed <- parseBool boardCollapsed
    pure . Board boardName boardId boardConfigs collapsed $ listToCards boardCards

parseBoards :: Block -> ParseResult [Board]
parseBoards block =
  do
    BulletList triples <- pure block
    unfoldM chomp triples
    where
      chomp [] = pure Nothing
      chomp xs = let
        ([prelude, blocks], rest) = splitAt 2 xs
        in (\r -> Just (r, rest)) <$> parseBoard blocks