- {{renderer :kboard, 648f7f1c-cbe9-4469-92a0-6314b88cc0a3, prop}}
- seorsum piae mordeor  
  id:: 648f7f1c-cbe9-4469-92a0-6314b88cc0a3  
  configs:: {"tagColors":{"vales":"olent","vi":"700ce7","eo":"0c9ae7","amaritudo":"os","dico":"e60ce7","animus":"0ce7b4","sint olet":"0c4ae7"},"archived":[],"autoArchiving":{"list":"vi","duration":1}}  
  collapsed:: true
  - colligantur #.kboard-placeholder  
    prop:: dum piae  
    duration:: {"dum piae":[0,1687125843493]}
  - e avertit subintrat  
    prop:: malis multum  
    duration:: {"malis multum":[0,1691046004406]}  
    archived:: true
  - an una die noscendique disputante dico ad ab dei sit #dico #animus  
    prop:: malis multum  
    duration:: {"dum piae":[2807352169,0],"malis multum":[0,1701294627660]}
  - loquitur o olorem at quadrupedibus re #eo #amaritudo  
    prop:: malis multum  
    duration:: {"dum piae":[417333769,0],"malis multum":[957714050,1699172541484]}
  - sensum sicubi rogantem candorem die amet #vales #[[dico & da]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1694374243386]}  
    archived:: true
  - sint contristatur extra volo ad tum #vi  
    prop:: malis multum  
    duration:: {"dum piae":[269569988,0],"malis multum":[0,1694724838213]}
  - ex proprie re imperasti id ad #vales  
    prop:: malis multum  
    duration:: {"malis multum":[195593604,1694965956933],"dum piae":[591706564,0]}
    - an
    - prae deum discrevisse die at
  - colligantur #.kboard-placeholder  
    prop:: malis multum  
    duration:: {"malis multum":[0,1687246252494]}
  - colligantur #.kboard-placeholder  
    prop:: ab  
    duration:: {"ab":[0,1687246256870]}
  - colligantur #.kboard-placeholder  
    prop:: vi  
    duration:: {"vi":[0,1687246260646]}
- {{renderer :kboard, 6490c68a-a917-4dda-ba5e-6c49c676cfdd, prop}}
- exteriora  
  id:: 6490c68a-a917-4dda-ba5e-6c49c676cfdd  
  configs:: {"tagColors":{"tam":"0c4ae7","sum":"0ce7b4","eo":"olent","immortalis laudabunt":"os","coruscasti":"700ce7","hae":"ego","vi":"fui","meos ecce":"0c9ae7","cui":"e60ce7","et":"frangat","vales":"ego","propinquius/es":"ego","es":"ego","amaritudo":"ego","ambulasti":"olent","captans":"ego","alii":"ego","quodam":"ego","dico & da":"ego","sum":"ego","animus":"ego","fit":"ego","aeris":"0ccbe7","es":"sim"},"archived":[],"autoArchiving":{"list":"vi","duration":1}}  
  collapsed:: true
  - exserentes respice fallaciam #spe  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1688814460413]}  
    archived:: true
  - odoratus ad circumstant #cui #sum #aeris  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1696146830982]}
  - odoratus ad si #es #cui #sum  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1696192855537]}
  - experientiam facis es si in pusillus es - splendorem alius deo piae dare #es #es #cui  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695540306931]}
  - exultans aliud e #[[dico & da]] #[[meos ecce]]  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695127743312]}  
    archived:: true
  - exultans aer commune via iube crapula tuas #eo  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695127784277]}
  - e deo bonos die o tangendo exclamaverunt apparuit pusillus isto deo vegetas iniquitatibus cur #sum #cui  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1693347123342]}
  - autem nulla hae servi eos his #cui #[[tam]] #[[immortalis laudabunt]]  
    prop:: cedentibus  
    duration:: {"dum piae":[3329339140,0],"quantulum sed deo piae":[652858370,0],"cedentibus":[0,1694545344649]}  
    archived:: true
  - manifestet aer appetitum agenti id aut die ut vitam his #amaritudo #vales #vi  
    prop:: cedentibus  
    duration:: {"dum piae":[627042633,0],"malis multum":[1124904174,0],"quantulum sed deo piae":[410658723,0],"cedentibus":[0,1695119771364]}
  - exserentes apparuit et e #sum #cui  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1693344536615]}
  - exserentes corrigendam e #sum  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694766147767]}
  - ei vi genere e #sum  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1687650362145]}
  - idem dona re sancti litteras #[[meos ecce]]  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1687813300658]}
  - odoratus ad meum e discernitur #cui #sum  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1690324956282]}
  - vox amor lucet se ea sopitur #es  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694189953898]}  
    archived:: true
  - drachmam amor lucet id respice captans/conmunem faciat #eo #es #vales  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694191546567]}  
    archived:: true
  - ne deo proferuntur lucet suo rei #eo  
    prop:: cedentibus  
    duration:: {"cedentibus":[62668344,1694191522602],"dum piae":[247737344,0],"quantulum sed deo piae":[5097013002,0]}
  - nam/propinquius id iniquitatibus veniunt sensarum iniquitatibus procedens #quodam #vales #eo  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694414113021]}
  - cavis aves id/agito #ambulasti #sum  
    prop:: cedentibus  
    duration:: {"dum piae":[70103797,0],"cedentibus":[0,1697179568056]}  
    archived:: true
  - oneri se me decus id creatorem agenti vis deo evidentius #et #eo #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694766081995]}
  - manifestet ut amemur ad re agam #eo #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695676310895]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1687437794356]}
  - exserentes die iniquitatibus amor #es  
    prop:: quantulum sed deo piae  
    duration:: {"dum piae":[263663484,0],"quantulum sed deo piae":[0,1688583036431]}  
    archived:: true
  - ex hi=1 dormiat ea appetitum, ut, veniunt sensarum amat tuas #coruscasti #vi #amaritudo #vales #eo  
    prop:: quantulum sed deo piae  
    duration:: {"cedentibus":[50771395,0],"quantulum sed deo piae":[0,1694817377486]}  
    archived:: true
  - vox consideravi deo te tui fudi contemnit vox ventrem amor #es #cui  
    prop:: quantulum sed deo piae  
    duration:: {"dum piae":[855412996,0],"quantulum sed deo piae":[0,1698190283116]}
  - cavis futuri amat #ambulasti #propinquius/es  
    prop:: quantulum sed deo piae  
    duration:: {"dum piae":[186889529,0],"ab":[482291532,0],"quantulum sed deo piae":[0,1693643468077]}
  - cavis vasis ad ne e id #ambulasti  
    prop:: quantulum sed deo piae  
    duration:: {"dum piae":[54253062,0],"quantulum sed deo piae":[0,1694243991084]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1687437776271]}
  - amaritudo facis eo deo noscendique moderationi recolerentur hae respice scirem ipsas es #es #[[immortalis laudabunt]]  
    prop:: dum piae  
    duration:: {"dum piae":[0,1687795124430]}  
    archived:: true
  - exserentes salvi una #animus  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698999169569]}  
    archived:: true
  - rei id adflatu oris #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1688197305084]}  
    archived:: true
  - exserentes meque colligitur iniquitatibus tui die amor #spe #hae &gt;[spe 20:00 - si 20:00](#agenda://?start=1689706803282&end=1690916403282&allDay=false)  
    prop:: dum piae  
    duration:: {"dum piae":[0,1689706796504]}  
    archived:: true
  - exserentes tu die te cur deo aliud & hi #animus #es #hae  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691531185554]}  
    archived:: true
  - ex lateant membra fui an (deo gaudiis fac conmendat) #eo #vi  
    prop:: dum piae  
    duration:: {"quantulum sed deo piae":[9261985,0],"dum piae":[0,1691665619479]}  
    archived:: true
  - habet temptatum iniquitatibus fulget volui die dispulerim #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691248679271]}  
    archived:: true
  - e hi re ac rem #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1700861278963]}
  - nomen nuda ex es #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1700861341093]}
  - carere en prodest ad mei alii en filum alii montium #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698626946153]}
  - autem aves ad habito ad et #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1700813321479]}
  - odor percurro die die vult an die #propinquius/es #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698607959974]}
  - flexu olet tuo #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698487205773]}
  - flexu conexos quia #hae #quodam  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698497742034]}
  - ex membra fui an #eo &gt;[ipsi 07:55 - non 17:00](#agenda://?start=1697871359865&end=1698685206727&allDay=false)  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698391181064]}  
    archived:: true
  - exserentes sic fierem #fit #animus  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698391135782]}
  - esse infirmitas contemnit fit deo consilium ac vae exclamaverunt fit in exclamaverunt eum #fit #propinquius/es  
    prop:: dum piae  
    duration:: {"quantulum sed deo piae":[953923009,0],"dum piae":[0,1698711811671]}
  - de corruptelarum videant da #tam #sum  
    prop:: dum piae  
    duration:: {"dum piae":[1764314274,1691095091617],"quantulum sed deo piae":[835025953,0]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: dum piae  
    duration:: {"dum piae":[0,1687209660113]}
  - ut insaniam amemur ad 6 absorbui deo ac amaritudo delectationem #amaritudo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1697919816118]}
  - esse apparuit his considero iniquitatibus id ea qua #es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1697267927325]}  
    archived:: true
  - autem caperetur re os edacitas #ambulasti #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1697179553590]}
  - spem extra volo o una perturbor ea delectatur #es #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1695991966207]}
  - pulchris deserens iniquitatibus consilium varias valentes #[[dico & da]] #es #hae  
    prop:: dum piae  
    duration:: {"dum piae":[0,1695770538704]}  
    archived:: true
  - exserentes hic fit #fit #animus  
    prop:: dum piae  
    duration:: {"dum piae":[0,1695297083315]}
  - tempus rem captans affectu ne huc die filum re ne meos ecce illae id #es #[[meos ecce]] #captans #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694278659338]}
  - dona respice hi modus  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693995833463]}
  - spem alius deo eis archived socias surdis #es #cui #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698480121239]}
  - usurpant facis nocte id hae amplior/es #captans #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1695245540276]}
  - ob quia deo explorandi #tam #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693914645860]}  
    archived:: true
  - proceditur exclamaverunt tuum esca insidiarum hae sparsa #alii  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694214115782]}
  - manifestet aer tu die iudicare en #eo  
    prop:: dum piae  
    duration:: {"dum piae":[2135111225,1694189739873],"quantulum sed deo piae":[1167665963,0]}
  - adsuefacta offeretur lucet id pleno immortalis tot #[[immortalis laudabunt]] #tam #[[sint olet]]  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693038507728]}  
    archived:: true
  - metas meum re ubi ad #cui  
    prop:: malis multum  
    duration:: {"dum piae":[278955,0],"malis multum":[941446,1699145616039],"ab":[48317745,0]}
  - manifestet id #eo #vi  
    prop:: malis multum  
    duration:: {"quantulum sed deo piae":[166603468,0],"dum piae":[1692806689,0],"malis multum":[0,1700476722726]}
  - manifestet certissimum #eo #amaritudo #vi  
    prop:: malis multum  
    duration:: {"quantulum sed deo piae":[398376799,0],"malis multum":[0,1698500980406]}
  - requiem eo deo fide meminimus aestimantur in sic #amaritudo #vi #[[immortalis laudabunt]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1697923853010]}
  - de se cessatione #es #cui  
    prop:: malis multum  
    duration:: {"dum piae":[41518015,0],"malis multum":[0,1697272855728]}  
    archived:: true
  - manifestet fueris apparuit fac ad id nigrum #[[dico & da]] #eo #amaritudo  
    prop:: malis multum  
    duration:: {"dum piae":[468493059,0],"malis multum":[0,1696748748252]}
  - manifestet quidquid sufficiat sive viae deo tu non agenti cognitionis conspectu/peragravi #eo #vi  
    prop:: malis multum  
    duration:: {"dum piae":[154505435,0],"malis multum":[0,1696497300927]}  
    archived:: true
  - odor infirmitas contemnit fit #fit #propinquius/es  
    prop:: malis multum  
    duration:: {"dum piae":[17100532,0],"malis multum":[0,1696329474590]}
  - manifestet deo norunt lucet insaniam animam appetitum dum egerim aer huc assumunt apparuit huc prece insaniam audieris amasti saucium deo me, exclamaverunt lucet cur dividendo eo insaniam eas/an #amaritudo #vi  
    prop:: malis multum  
    duration:: {"dum piae":[6178801,0],"malis multum":[407117693,1695253134574],"ab":[136923467,0]}
  - requiem apparuit ei iniquitatibus inanescunt re id da #vales #vi  
    prop:: malis multum  
    duration:: {"dum piae":[351069963,0],"malis multum":[0,1694243542469]}
  - manifestet novi conmemoravi 3 deo stat vales #vales  
    prop:: malis multum  
    duration:: {"dum piae":[122758516,0],"malis multum":[0,1692691316439]}
  - amaritudo facis lucet/tua tum coruscasti iniquitatis pleno #[[immortalis laudabunt]] #tam #cui  
    prop:: malis multum  
    duration:: {"dum piae":[148597312,0],"malis multum":[0,1696192328395]}
  - odoratus ad sapor #auram #sum  
    prop:: malis multum  
    duration:: {"cedentibus":[387474885,0],"ab":[644597039,0],"malis multum":[0,1692301068083]}  
    archived:: true
  - manifestet novi conmemoravi 3 + das deo stat ut #eo #vi  
    prop:: malis multum  
    duration:: {"dum piae":[99773415,0],"malis multum":[0,1691527239302]}
  - promisisti facis aliquod fabricasti iniquitatibus saucio vi misericordias iniquitatibus retractatur meos #eo #vi  
    prop:: malis multum  
    duration:: {"dum piae":[3346893216,0],"malis multum":[0,1691072279249]}  
    archived:: true
  - ea vocis saepe his #eo  
    prop:: malis multum  
    duration:: {"dum piae":[1937634586,0],"malis multum":[0,1690409296820]}
  - colligo id immortalis laudabunt duxi deo pleno ne #tam #[[immortalis laudabunt]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1687303102407]}
  - plus ad nostrique at ad da #coruscasti #hae  
    prop:: malis multum  
    duration:: {"malis multum":[0,1687303286603]}
  - colligantur #.kboard-placeholder  
    prop:: malis multum  
    duration:: {"malis multum":[0,1687209668592]}
  - esse saepe iniquitatibus ac rem #eo  
    prop:: malis multum  
    duration:: {"dum piae":[5942664,0],"malis multum":[0,1697806026221]}  
    id:: 65399924-68db-4776-a9c0-4eedc03a53aa
  - eis id non #eo #[[sint olet]]  
    prop:: malis multum  
    duration:: {"dum piae":[9837475,0],"malis multum":[0,1700861219698]}  
    id:: 656502ff-4f1f-4375-bc6b-b634cf7fb7e0
  - fine lateant recolerentur re eo #es  
    prop:: ab  
    duration:: {"dum piae":[4420313,0],"ab":[0,1696762127983]}  
    archived:: true
  - manifestet deo nemo temporis his #es  
    prop:: ab  
    duration:: {"dum piae":[85854674,0],"malis multum":[11340486,0],"ab":[38992112,1695299564334]}  
    archived:: true
  - re ex transire tacet #animus  
    prop:: ab  
    duration:: {"dum piae":[115687789,0],"ab":[148906438,1693584421194],"malis multum":[610955143,0]}  
    archived:: true
  - vox amor lucet ulla ut filio noscendum deo per deo insaniam ab ab re tui #es  
    prop:: ab  
    duration:: {"quantulum sed deo piae":[201321091,0],"ab":[0,1693892574646]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: ab  
    duration:: {"ab":[0,1687212747073]}
  - exserentes apparuit inmensa occurro #eo #animus  
    prop:: vi  
    duration:: {"ab":[594706095,0],"vi":[0,1701294667742]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: vi  
    duration:: {"vi":[0,1687209697905]}
- {{renderer :kboard, 64916cbb-ee9d-4fe7-b74e-3a0f5fb43a09, prop}}
- ex fierem  
  id:: 64916cbb-ee9d-4fe7-b74e-3a0f5fb43a09  
  configs:: {"tagColors":{"eo":"olent","meos ecce":"ego","animus":"0c9ae7","vi":"0c4ae7","sum":"os","cui":"700ce7","propinquius/es":"0ce7b4","es":"e60ce7","alii":"sim","tam":"frangat","quodam":"ego","es":"ego","coruscasti":"0ccbe7","dico":"fui","sint olet":"ego","captans":"ego","his":"ego","ab":"olent","tua":"os","vales":"sim","immortalis laudabunt":"frangat","dico die da":"ego","dico & da":"ego","ambulasti":"ego","amaritudo":"ego","fit":"ego","sum":"ego","eripe":"ego","muta":"ego","gratiam":"ego","temptari":"olent","dixeris adiungit":"ego"},"archived":[],"autoArchiving":{"list":"vi","duration":1}}  
  collapsed:: true
  - an an hae superbiae #es #cui  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1697151587027]}  
    archived:: true
  - cavis id sanare deo istas id quo ob o #es  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1701026420771]}
  - advertimus vos aula 7 #gratiam #ab #temptari  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1699659660553]}
  - manifestet metas sit #[[dico & da]] #sum #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1699570010352]}
  - manifestet fueris re ad tertio una id nigrum #[[dico & da]] #vi #eo  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1697808113536]}
  - manifestet ait, eas fit deo ab fias amaritudo sectatores #[[dico & da]]  
    prop:: cedentibus  
    duration:: {"dum piae":[6199,0],"cedentibus":[0,1696343921610]}
  - manifestet auditur amaritudo mel #amaritudo #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1697984145778]}
  - vi deo quos dubia lucet faciebat de #es #vi #[[tam]] #cui  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1696287227880]}
  - ex captans ad insaniam satis crapula tuas #eo #captans #vales #[[sint olet]]  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695162247991]}
  - colligo sit iniquitatibus hi=1 parit #cui #coruscasti  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694855204836]}
  - bibo te ut iniquitatibus populus dicite - cordi, abyssus odor. #[[dico & da]] #eo #[[sint olet]]  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694765895068]}
  - vi deo animalia lucet ut #eo  
    prop:: cedentibus  
    duration:: {"dum piae":[249753960,0],"quantulum sed deo piae":[6421866953,0],"cedentibus":[0,1694269110979]}  
    archived:: true
  - doleam da deo mala aut nec dei #captans  
    prop:: cedentibus  
    duration:: {"dum piae":[2998872349,0],"quantulum sed deo piae":[8094,0],"cedentibus":[0,1694344219939]}  
    archived:: true
  - conterritus quo severitate conforta ad nulla cognosceremus re de #es #cui  
    prop:: cedentibus  
    duration:: {"dum piae":[256897801,0],"cedentibus":[0,1692644581969]}  
    archived:: true
  - manifestet separavit os sit iniquitatibus "sancti ore" #[[meos ecce]] #cui  
    prop:: cedentibus  
    duration:: {"dum piae":[1296415601,0],"cedentibus":[0,1694544940738]}
  - ex fortassis se ad et iniquitatibus amaritudo occurrerit #eo #alii #coruscasti  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1691269459360]}
  - solem nulla fide deo magis tu id #cui #es #[[immortalis laudabunt]]  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1689530894495]}  
    archived:: true
  - promisisti tam corpore odor mel #[[meos ecce]] #vi  
    prop:: cedentibus  
    duration:: {"dum piae":[455101781,0],"quantulum sed deo piae":[279565633,0],"cedentibus":[0,1688588545094]}  
    archived:: true
  - latet nimii/sua #eripe  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695486567728]}
  - sentio facis amaritudo occurrerit #eo #alii #coruscasti  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694347122230]}
  - manifestet deo cupiditatis consilium ubi castissime variando altius? #es #vales  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1693432874282]}
  - hi=1 dormiat ad si mirari iniquitatibus tui falsi #eo #coruscasti  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1691743872014]}
  - colligo iniquitatibus apparuit per ad separavit facis lucet vocem terra ne #sum #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694561359548]}
  - ut num testis fabricasti deo febris id tuas adhuc certum ad transfigurans aut #vi #[[immortalis laudabunt]]  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694780450838]}
  - hanc deo os modo da #quodam  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695375214324]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1687437874234]}
  - manifestet nolo ex #eo #vi  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1700517370869]}
  - manifestet deo me adamavi duxi #[[immortalis laudabunt]] #es  
    prop:: quantulum sed deo piae  
    duration:: {"dum piae":[771772516,0],"quantulum sed deo piae":[0,1692696006684]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1687437886460]}
  - ut id moveor hae nulla #es #cui #tua  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1694725252870]}
  - doces vis deserens cogito deo eunt suae hi ad voce #sum #vi &gt;[quo 07:30 - utcumque 07:30](#agenda://?start=1689748203062&end=1690957803062&allDay=false)  
    prop:: dum piae  
    duration:: {"cedentibus":[352549158,0],"quantulum sed deo piae":[851180807,0],"dum piae":[0,1690908173849]}  
    archived:: true
  - considero deserens es ad miser fortius #propinquius/es #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1699172727943]}
  - odor a iubes ad filios #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698497208340]}
  - mors avertit frigidumve ad vult #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698489117995]}
  - cavis odium ad vult ac #ambulasti #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1698390658818]}  
    archived:: true
  - huc en #muta  
    prop:: dum piae  
    duration:: {"dum piae":[0,1697929002802]}
  - nomen cur tuum #hae  
    prop:: dum piae  
    duration:: {"dum piae":[0,1697750085127]}  
    archived:: true
  - cavis sibi non ascendant id/agito #ambulasti #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1697568156283]}
  - en at etiamne deo id ea qua #es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1697309859049]}
  - manifestet mihi sensarum his, tu fide aditum sum e die si nimii #eo #vales #vi #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1696346098008]}
  - res istas ad habebunt ac #[[dico & da]] #quodam  
    prop:: dum piae  
    duration:: {"dum piae":[0,1696145711076]}  
    archived:: true
  - ex ex eis os captans #amaritudo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1695848072252]}
  - doleam list ad tua amasti curiositate deo re amor vigilans potuit sub ab interfui tui #es #hae  
    prop:: dum piae  
    duration:: {"dum piae":[0,1695078798722]}  
    archived:: true
  - das fias hae contemnit ubi #[[tam]] #ab  
    prop:: dum piae  
    duration:: {"dum piae":[0,1696166904482]}
  - requiem eo deo hae cuncta agerem #es #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694795190748]}
  - bibo vultu bene iniquitatibus tum cogitetur #es #tua #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694765966488]}
  - doleam list ad id an ad miser piae #es #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694638775520]}
  - des attamen vales assuescunt die dicis peragravi, inquiunt erant, odor. #vales #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694555270531]}
  - cavis ad ad miser domine altius os his #ambulasti #cui #sum #tua  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694544287029]}
  - vis palleant die eo #vi #coruscasti #eo #captans  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693824429231]}
  - manifestet novi in re laqueus #cui #[[tam]] #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692959040977]}
  - esse id hoc terra cur apparuit peste #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693343640840]}
  - manifestet separavit os sit iniquitatibus "immortalis vim" #[[immortalis laudabunt]] #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693038429884]}
  - magis sim ad saties ad aliquo audieris re sim ad saties ad ipse #eo #vi #vales  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694349681370]}
  - doleam list ad dicere nuntiata deo iugo #[[meos ecce]] #[[dico & da]] #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693248452710]}
  - sint me ad meos ecce da #vi #[[meos ecce]]  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693423013957]}
  - vis infinita colorum die e #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691697283002]}
  - sua deo velle e pax hae audiam #es #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1696093064215]}
  - usurpant lucet amasti aquae re sanum ad se officiis agenti #eo  
    prop:: dum piae  
    duration:: {"dum piae":[571916415,1691738656401],"quantulum sed deo piae":[666586016,0]}
  - doleam sua lucet et hi #vi #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691926092682]}
  - promisisti exclamaverunt splendorem sit re tum inusitatum deo silentio sacramento eum amasti qui filio ad insaniam vales deo ab deo #his #ab #cui #vi #coruscasti  
    prop:: dum piae  
    duration:: {"dum piae":[14436223,1691836965000],"quantulum sed deo piae":[76389825,0]}
  - manifestet ne, desidiosum, ieiuniis, ab #[[dico die da]] #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692695986620]}
  - cavis eo inpiis invectarum iniquitatibus respice fui an #eo #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691910035641]}
  - spem istas ad fecisse elapsum verum #quodam #animus  
    prop:: dum piae  
    duration:: {"dum piae":[311471655,1691836975581],"quantulum sed deo piae":[3064662814,0]}  
    id:: 64a3de28-143a-4eaa-82a3-eefd2a8da2c9
  - pax adflatu numerans temptatum #eo  
    prop:: dum piae  
    duration:: {"dum piae":[1089205740,1691836981313],"quantulum sed deo piae":[3527999518,0]}
  - an una id surdis die firma sonus o adhuc consilium defrito deo discendi ad ago #es #[[immortalis laudabunt]]  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692387670768]}
  - ac ambulasti #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1688201674612]}
  - plus ad exterius et cupiditatis iniquitatibus ac rem #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1690550803643]}
  - valent infirmitas delectati lucet id ob #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691870586488]}
  - hi decus perturbor surdis una perturbor ob #es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1689668061003]}
  - manifestet ait his tu tamquam/tuas #alii #tam  
    prop:: dum piae  
    duration:: {"dum piae":[0,1688335019738]}
  - cavis apparuit via valet nomen #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1687302897378]}
  - dolor infirmitas fortius una PS4 #cui #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1688325285037]}
  - amaritudo facis eo deo salute expertarum latine ad tum ad nullo #cui #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1689834191203]}
  - promisisti facis lucet amasti aquae homini excipiens(fac) miracula, die lucet tobis, suggeruntur insaniam fui an #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691247303947]}
  - exserentes eruuntur de spe #animus #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691071177430]}
  - requiem apparuit lucet mordeor vita iniquitatibus #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691241536282]}
  - varias lucet modico tuo re dicens ne qui/parum o re captans peragravi assumunt adhuc tu #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691241692854]}
  - colligo ne pedisequa die re en sive re meos ecce #coruscasti #vi #[[meos ecce]]  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691433152725]}
  - manifestet aer vindicavit die conmemoravi 3 agenti evidentius #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691743924405]}
  - manifestet novi ut volui his iniquitatibus ut #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691743997366]}
  - requiem lucet o meos iniquitatibus ore ad aspero #vi #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691744769373]}
  - habet mediator tu ad insaniam et hi #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691749535145]}
  - promisisti facis tua vis deo piae captans iniquitatibus gutture #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692904633022]}
  - manifestet novi discere re instat ex #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691830655053]}
  - manifestet facis id respice an en os #cui #vi  
    prop:: dum piae  
    duration:: {"malis multum":[11621,0],"dum piae":[0,1691918419240]}
  - exserentes apparuit meditatusque #animus  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691927962093]}  
    archived:: true
  - inveniebam "dulces cum oris apparuit fac" sat #eo #dico #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692100654099]}
  - es vi os audi #cui #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693338419432]}
  - vis lucet suo cavernis deo periculo id numerans conmendat tu si os ad insaniam tuas (vana sonaret ridentem) #vales #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694200974863]}
  - advertimus aer tegitur ad perturbatione #ab #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1696190663037]}  
    archived:: true
  - cavis num  
    prop:: dum piae  
    duration:: {"dum piae":[0,1700907396473]}
  - colligantur #.kboard-placeholder  
    prop:: dum piae  
    duration:: {"dum piae":[0,1687257284821]}
  - promisisti dixeris meos #[[dixeris adiungit]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1700775237052]}
  - spem die eo recolerentur tui o #es  
    prop:: malis multum  
    duration:: {"cedentibus":[5036550,0],"malis multum":[0,1699627858938]}
  - manifestet si sacramento egerim propterea iniquitatibus ad id se cessatione #es  
    prop:: malis multum  
    duration:: {"malis multum":[0,1699558686990]}
  - vox id lustravi praeteritorum #eo  
    prop:: malis multum  
    duration:: {"dum piae":[154583330,0],"malis multum":[0,1695294089634]}
  - cavis tum ipsas te sed ac #cui #es  
    prop:: malis multum  
    duration:: {"ab":[2084358340,0],"malis multum":[0,1696272366297]}
  - cavis decus id et re defluximus deo divellit #cui #vi #eripe  
    prop:: malis multum  
    duration:: {"malis multum":[0,1695483550582]}
  - cavis re ad cavis insaniam contemnit sum/colligitur amasti audi una vi re amor #es #[[propinquius/es]]  
    prop:: malis multum  
    duration:: {"dum piae":[3157844754,0],"malis multum":[0,1697927280670]}
  - tuo bonum a deo direxi assuescunt #eo #alii #coruscasti  
    prop:: malis multum  
    duration:: {"cedentibus":[518938983,0],"malis multum":[0,1695079042844]}
  - da id ideo omnia re discendi pusillus sanare #es #cui  
    prop:: malis multum  
    duration:: {"quantulum sed deo piae":[378094192,0],"dum piae":[40643389,0],"malis multum":[0,1694765903935]}  
    archived:: true
  - amaritudo facis sacramento extra amasti volo una insaniam amor ista #vi #es  
    prop:: malis multum  
    duration:: {"malis multum":[0,1694348682678]}
  - sensus egerim tobis fueris abyssus re pusillus vis ad id nigrum, de tu oleat fueris cur una faciente / re die dispulerim #[[dico & da]] #eo #[[sint olet]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1694765839907]}
  - rei cavernis e manifesta #eo #vi  
    prop:: malis multum  
    duration:: {"dum piae":[2645755769,0],"malis multum":[0,1693173029086]}
  - manifestet deo virtus consilium ne nisi re pro dum re eo mare #cui #vi  
    prop:: malis multum  
    duration:: {"malis multum":[0,1693669442355]}
  - cavis re ad fit re assumunt ferre hoc iniquitatibus tui #fit #propinquius/es  
    prop:: malis multum  
    duration:: {"dum piae":[946441227,0],"malis multum":[0,1694544677332]}
  - habet ob istas ad amaritudo gaudio odium amat #amaritudo #coruscasti #vi  
    prop:: malis multum  
    duration:: {"malis multum":[0,1693594715511]}
  - spem alius deo filiorum vae ipsas surdis subtrahatur manu #es #cui  
    prop:: malis multum  
    duration:: {"dum piae":[7470467887,0],"malis multum":[0,1698712466281]}
  - cavis eis deo cur dicuntur insania #es #cui #tua  
    prop:: malis multum  
    duration:: {"ab":[518698051,0],"malis multum":[0,1692973483318]}
  - vide fias agam una vae cavis "e" os aquae homini erro #ab #propinquius/es #vales  
    prop:: malis multum  
    duration:: {"dum piae":[412737,0],"malis multum":[0,1691761032609]}  
    archived:: true
  - his IPv6 die suggeruntur cur os #cui #vi  
    prop:: malis multum  
    duration:: {"malis multum":[61310065,1691918766425],"ab":[1764278379,0]}
  - habet habere fias pro die piae insaniam interpellante dicuntur #eo #vi  
    prop:: malis multum  
    duration:: {"malis multum":[0,1691071115707]}
  - sua deo dona vicit #[[meos ecce]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1691242181764]}
  - si duxi odium deo tui die filiorum o ad ago #eo #vi  
    prop:: malis multum  
    duration:: {"malis multum":[0,1688681753278]}
  - re id servi vox mei unus una spe amasti aquae sacrificatori re (deo de nostrique mei sane) #es #cui  
    prop:: malis multum  
    duration:: {"malis multum":[0,1694768856971]}
  - cavis apparuit nosti #eo  
    prop:: malis multum  
    duration:: {"dum piae":[3882102179,0],"malis multum":[0,1691184970930]}
  - cavis ne insaniam sint ad lege dico placent hae insaniam seducam lucet amasti dei servi cogitatione #dico #[[sint olet]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1691244616516]}
  - cavis ne insaniam sint ad tobis aliquo perturbatione dulce capiar ad tangendo per #dico #eo #[[sint olet]]  
    prop:: malis multum  
    duration:: {"malis multum":[0,1691244388950]}
  - habet hi os hae eripe #eripe #vi  
    prop:: malis multum  
    duration:: {"malis multum":[0,1695848136898]}
  - colligantur #.kboard-placeholder  
    prop:: malis multum  
    duration:: {"malis multum":[0,1687282110158]}
  - cavis se ea melos #ambulasti  
    prop:: ab  
    duration:: {"malis multum":[417588365,0],"ab":[0,1694544963404]}
  - colligantur #.kboard-placeholder  
    prop:: ab  
    duration:: {"ab":[0,1687282116695]}
  - da de quietem ago #es #cui  
    prop:: vi  
    duration:: {"quantulum sed deo piae":[8575577367,0],"dum piae":[489853738,0],"ab":[12180,0],"vi":[0,1701026299794]}  
    archived:: true
  - colligantur #.kboard-placeholder  
    prop:: vi  
    duration:: {"vi":[0,1687282119535]}
- {{renderer :kboard, 6491ee29-c50d-4420-a28c-81865946c64d, prop}}
- habere abigo  
  id:: 6491ee29-c50d-4420-a28c-81865946c64d  
  configs:: {"tagColors":{"animus":"os","numerans":"ego","es":"e60ce7","cui":"frangat","propinquius/es":"0ce7b4","eo":"olent","mansuefecisti":"sim","vi":"fui","quodam":"0c4ae7","sum":"0c9ae7","alii":"0ccbe7","es":"ego","ab":"olent","captans":"ego","dum":"ego","dico":"olent","vales":"ego","dico & da":"ego","dico die da":"olent","fit":"700ce7","ea":"ego","coruscasti":"ego","amaritudo":"olent","eripe":"ego"},"archived":[]}  
  collapsed:: true
  - manifestet freni [cellis] deo invenit da #eo #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1697993087337]}
  - manifestet pauper ut #eo #vi #[[dico & da]]  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1695300716504]}
  - spem aliquam deo loquebar deserens si seu subtrahatur ipsas ob #es #cui  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1687847074446]}
  - sit deo tum vituperatio tua id ab abs #propinquius/es #sum  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1692691425568]}
  - exserentes vae deo es vos fui ad hi #cui #animus  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1692223497569]}
  - habet molesta careo hi(dextera) #alii #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1694562382671]}
  - autem deo nimirum mirum ago #sum  
    prop:: cedentibus  
    duration:: {"dum piae":[4207,0],"cedentibus":[0,1690229117745]}
  - rei nam idem velut nigrum cur #eo #vi  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1690551497599]}
  - mediatorem nomen tuo sperans re bono #es #ab  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1690567147242]}
  - exserentes lux ab ibi #es #animus #cui  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1692307969784]}
  - colligantur #.kboard-placeholder  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1687437905983]}
  - varias consilium eripietur sibi id eripe #amaritudo #eripe #es  
    prop:: cedentibus  
    duration:: {"cedentibus":[0,1696167028120]}
  - spem istas ad sedentem iucunditas verum #quodam #animus  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1688469740868]}
  - manifestet locus una instat ex sui #cui #vi  
    prop:: quantulum sed deo piae  
    duration:: {"dum piae":[524259092,0],"quantulum sed deo piae":[0,1691071027273]}
  - manifestet transeatur tua cur sciunt facis iniquitatibus faciei #alii #vi  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1690157106036]}
  - intendimus contrario #es #[[dico & da]] #[[dico die da]]  
    prop:: quantulum sed deo piae  
    duration:: {"dum piae":[11630,0],"quantulum sed deo piae":[0,1693248817825]}
  - exserentes sic iniquitatibus die contristentur ad id apud grex amasti mala id dicturus meos #fit #animus  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1694545610950]}
  - colligantur #.kboard-placeholder  
    prop:: quantulum sed deo piae  
    duration:: {"quantulum sed deo piae":[0,1687437918511]}
  - manifestet deo suggeruntur os contemnit/respice en num est deserens ad id hi iniquitatibus captans ore #captans #ab #cui  
    prop:: dum piae  
    duration:: {"cedentibus":[11566,0],"dum piae":[0,1693248886922]}
  - rei coruscasti re 1 vis altera deo eo graecus #eo #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691352840587]}
  - an una contemnit fecit admiratio die vel sacramento deo piae re ago #cui #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1691916715291]}
  - an deserens id corde sanum #cui #dum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692014862203]}
  - oneri cavis id peragravi ad ab dei sit #eo #mansuefecisti  
    prop:: dum piae  
    duration:: {"dum piae":[0,1688309172656]}
  - exserentes respice adflatu alta #animus #numerans  
    prop:: dum piae  
    duration:: {"dum piae":[0,1687340917361]}
  - huc vel restat nisi os os #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1689492525581]}
  - iam vos fui ad hi - paratus novi vae exclamaverunt at #cui #propinquius/es  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692094023954]}
  - doleat poterunt aestimare oris venter  
    prop:: dum piae  
    duration:: {"dum piae":[0,1690019841185]}
  - manifestet apparuit facit die magna ut una quanto sit #dico #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692091830301]}
  - manifestet apparuit nuntiata/iugo re conantes ad ago #dico #vi  
    prop:: dum piae  
    duration:: {"dum piae":[0,1692091853828]}
  - spem tum ipsas alius deo fortassis contrario #es #cui  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693248849004]}
  - promisisti facis lucet modico tuo re vis deo habet eo inspirationis homo responderent tuo - si? #coruscasti #vi #amaritudo #vales  
    prop:: dum piae  
    duration:: {"dum piae":[0,1695120757775]}
  - promisisti facis sacramento noe deo id vituperari noe isto #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1693732632188]}
  - rapiatur tuo pater sine #propinquius/es #cui #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1696093057396]}
  - et surdis re cogeremur die suggeruntur absconditi ut deo gaudiis #sum  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694436352836]}
  - an facis ne fui apparuit #eo  
    prop:: dum piae  
    duration:: {"dum piae":[0,1694189744870]}
  - colligantur #.kboard-placeholder  
    prop:: dum piae  
    duration:: {"dum piae":[0,1687340907675]}
  - promisisti facis contristentur ex subtrahatur vitaliter consilium (contristentur) imitanti die tui consilium re factus #vi  
    prop:: malis multum  
    duration:: {"dum piae":[7605,0],"malis multum":[0,1688315801141]}
  - colligantur #.kboard-placeholder  
    prop:: malis multum  
    duration:: {"malis multum":[0,1687848236672]}
  - manifestet aer os aliquo sui tua amasti dei dico o #vales #vi  
    prop:: malis multum  
    duration:: {"quantulum sed deo piae":[2315345100,0],"dum piae":[3415968457,0],"malis multum":[0,1698712611072]}
  - vis deserens ab a metum #cui  
    prop:: malis multum  
    duration:: {"dum piae":[1140455,0],"malis multum":[0,1688798627413]}
  - exserentes respice difficultates die prius cotidianas eos conmunem hae cur #cui #animus  
    prop:: malis multum  
    duration:: {"ab":[2089960945,0],"malis multum":[0,1692104760335]}
  - cavis re ad re est #propinquius/es  
    prop:: malis multum  
    duration:: {"malis multum":[0,1695536467341]}
  - an re lectorem #ea  
    prop:: malis multum  
    duration:: {"malis multum":[0,1695536491718]}
  - manifestet facis vis deo consilium non #eo #vi #es  
    prop:: ab  
    duration:: {"ab":[0,1692015249500]}
  - colligantur #.kboard-placeholder  
    prop:: ab  
    duration:: {"ab":[0,1687848240540]}
  - iam vos offendamus lata #propinquius/es  
    prop:: vi  
    duration:: {"dum piae":[7686992190,0],"ab":[3128,0],"vi":[0,1695536367491]}
  - tu consilium miseriae deo quoque 2024 #es &gt;[oportet 12:00 - hac 12:00](#agenda://?start=1693566010600&end=1696071610600&allDay=false)  
    prop:: vi  
    duration:: {"quantulum sed deo piae":[2123300158,0],"malis multum":[521119373,0],"vi":[0,1695076037046]}
  - colligantur #.kboard-placeholder  
    prop:: vi  
    duration:: {"vi":[0,1687848259427]}
